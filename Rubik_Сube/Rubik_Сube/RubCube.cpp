#include "StdAfx.h"
#include "RubCube.h"
#include "Box.h"

RubCube::RubCube(void)
{
	initFace();
}


RubCube::~RubCube(void)
{
}


void RubCube::assemble(void)
{
}


void RubCube::confuse(void)
{
}



int RubCube::printFaceState(void)
{
	return 0;
}


int RubCube::initFace(void)
{
		int i, j, k;
		// FACE_TOP
		for(j = 0; j < 3; j++)
			for(k = 0; k < 3; k++)
				matrixBox[0][j][k].setColorFace(Box::FACE_TOP,Box::COLOR_WHITE);
 
		// FACE_BOTTOM
		for(j = 0; j < 3; j++)
			for(k = 0; k < 3; k++)
				matrixBox[2][j][k].setColorFace(Box::FACE_BOTTOM,Box::COLOR_YELLOW);
 
		// FRONT
		for(i = 0; i < 3; i++)
			for(j = 0; j < 3; j++)
				matrixBox[i][j][0].setColorFace(Box::FACE_FRONT,Box::COLOR_BLUE);
 
		// FACE_BACK
		for(i = 0; i < 3; i++)
			for(j = 0; j < 3; j++)
				matrixBox[i][j][2].setColorFace(Box::FACE_BACK,Box::COLOR_BROWN);
 
		// FACE_RIGHT
		for(i = 0; i < 3; i++)
			for(k = 0; k < 3; k++)
				matrixBox[i][2][k].setColorFace(Box::FACE_RIGHT,Box::COLOR_GREEN);
 
		// FACE_LEFT
		for(i = 0; i < 3; i++)
			for(k = 0; k < 3; k++)
				matrixBox[i][0][k].setColorFace(Box::FACE_LEFT,Box::COLOR_RED);
 
	return 0;
}
