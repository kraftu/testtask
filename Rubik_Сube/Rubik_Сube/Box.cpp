#include "StdAfx.h"
#include "Box.h"
#include <string.h>


/*
(�������, �����,�����, ����, ����, ���)
unsigned int faceColor[6]; 
*/

#define INVALID_INDEX "Invalid index facets!"
#define INVALID_COLOR "Invalid color facets!"

Box::Box(void)
{
	memset(faceColor,0,sizeof(faceColor));
}


Box::~Box(void)
{

}
//rotate YoZ
void Box::rotateX(){
	int tmp=faceColor[FACE_FRONT];
	faceColor[FACE_FRONT] = faceColor[FACE_RIGHT];
	faceColor[FACE_RIGHT] = faceColor[FACE_BACK];
	faceColor[FACE_BACK] = faceColor[FACE_LEFT];
	faceColor[FACE_LEFT] = tmp;

}
//rotate ZoX
void Box::rotateY(){
	int tmp=faceColor[FACE_FRONT];
	faceColor[FACE_FRONT] = faceColor[FACE_TOP];
	faceColor[FACE_TOP] = faceColor[FACE_BACK];
	faceColor[FACE_BACK] = faceColor[FACE_BOTTOM];
	faceColor[FACE_BOTTOM] = tmp;
}
//rotate XoY
void Box::rotateZ(){
	int tmp=faceColor[FACE_TOP];
	faceColor[FACE_TOP] = faceColor[FACE_LEFT];
	faceColor[FACE_LEFT] = faceColor[FACE_BOTTOM];
	faceColor[FACE_BOTTOM] = faceColor[FACE_RIGHT];
	faceColor[FACE_RIGHT] = tmp;
}

void Box::setColorFace(int indexFace, int color)
{
	if(0>indexFace || indexFace>5)  throw INVALID_INDEX ;
	if(0>color || color>6)  throw INVALID_COLOR ;
	faceColor[indexFace]=color;
}


int Box::getColorFace(int indexFace)
{
	if(0>indexFace || indexFace>5) throw INVALID_INDEX;
	return faceColor[indexFace];
}


int Box::printStateFace(void)
{
	printf("FRONT:%d RIGHT:%d FACE_BACK:%d FACE_LEFT:%d FACE_TOP:%d BOTTOM:%d\n",faceColor[FACE_FRONT],faceColor[FACE_RIGHT],faceColor[FACE_BACK],faceColor[FACE_LEFT],faceColor[FACE_TOP],faceColor[FACE_BOTTOM]);
	return 0;
}
