#pragma once
#include "Box.h"
class RubCube
{
public:
	RubCube(void);
	~RubCube(void);
	void assemble(void);
	void confuse(void);
protected:
	Box matrixBox[3][3][3];
	int printFaceState(void);
	int initFace(void);

};

