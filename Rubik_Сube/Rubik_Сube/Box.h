#pragma once


class Box
{
protected:
	//(�����,�����,�����, ����, ����, ���)
	unsigned int faceColor[6]; 

public:
	static const int COLOR_WHITE=1;
	static const int COLOR_GREEN=2;
	static const int COLOR_BLUE=3;
	static const int COLOR_RED=4;
	static const int COLOR_YELLOW=5;
	static const int COLOR_BROWN=6;

	static const int FACE_FRONT=0;
	static const int FACE_RIGHT=1;
	static const int FACE_BACK=2;
	static const int FACE_LEFT=3;
	static const int FACE_TOP=4;
	static const int FACE_BOTTOM=5;

	Box(void);
	~Box(void);
	//rotate YoZ
	void rotateX(void);
	//rotate ZoX
	void rotateY(void);
	//rotate XoY
	void rotateZ(void);
	void setColorFace(int indexFace, int color);
	int getColorFace(int indexFace);

	int printStateFace(void);
};

